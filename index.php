<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">

    <!--Page Title-->
    <title>Gojek - SME Dashboard</title>

    <!--Meta Keywords and Description-->
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no"/>

    <!--Favicon-->
    <link rel="shortcut icon" href="images/favicon.ico" title="Favicon"/>

    <!-- Main CSS Files -->
    <link rel="stylesheet" href="css/style.css">

    <!-- Namari Color CSS -->
    <link rel="stylesheet" href="css/namari-color.css">

    <!--Icon Fonts - Font Awesome Icons-->
    <link rel="stylesheet" href="css/font-awesome.min.css">

    <!-- Animate CSS-->
    <link href="css/animate.css" rel="stylesheet" type="text/css">

    <!--Google Webfonts-->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
    
    <!-- Bootstrap --> 
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    
    <!-- Datatables --> 
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" />

</head>
<body>

<!-- Preloader -->
<div id="preloader">
    <div id="status" class="la-ball-triangle-path">
        <div></div>
        <div></div>
        <div></div>
    </div>
</div>
<!--End of Preloader-->

<div class="page-border" data-wow-duration="0.7s" data-wow-delay="0.2s">
    <div class="top-border wow fadeInDown animated" style="visibility: visible; animation-name: fadeInDown;"></div>
    <div class="right-border wow fadeInRight animated" style="visibility: visible; animation-name: fadeInRight;"></div>
    <div class="bottom-border wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;"></div>
    <div class="left-border wow fadeInLeft animated" style="visibility: visible; animation-name: fadeInLeft;"></div>
</div>

<div id="wrapper">

    <header id="banner" class="scrollto clearfix" data-enllax-ratio=".5">
        <div id="header" class="nav-collapse">
            <div class="row clearfix">
                <div class="col-1">

                    <!--Logo-->
                    <div id="logo">

                        <!--Logo that is shown on the banner-->
                        <img src="images/logo.png" id="banner-logo" alt="Landing Page"/>
                        <!--End of Banner Logo-->

                        <!--The Logo that is shown on the sticky Navigation Bar-->
                        <img src="images/logo-2.png" id="navigation-logo" alt="Landing Page"/>
                        <!--End of Navigation Logo-->

                    </div>
                    <!--End of Logo-->

                    <aside>

                        <!--Social Icons in Header-->
                        <ul class="social-icons">
                            <li>
                                <a target="_blank" title="Facebook" href="https://www.facebook.com/gojekindonesia">
                                    <i class="fa fa-facebook fa-1x"></i><span>Facebook</span>
                                </a>
                            </li>
                            <li>
                                <a target="_blank" title="Twitter" href="http://www.twitter.com/gojekindonesia">
                                    <i class="fa fa-twitter fa-1x"></i><span>Twitter</span>
                                </a>
                            </li>
                            <li>
                                <a target="_blank" title="Instagram" href="http://www.instagram.com/gojekindonesia">
                                    <i class="fa fa-instagram fa-1x"></i><span>Instagram</span>
                                </a>
                            </li>
                        </ul>
                        <!--End of Social Icons in Header-->

                    </aside>

                    <!--Main Navigation-->
                    <nav id="nav-main">
                        <ul>
                            <li>
                                <a href="#home">Home</a>
                            </li>
                            <li>
                                <a href="#emotion">Emotion</a>
                            </li>
                            <li>
                                <a href="#featurebasedemotion">Feature-based Emotion</a>
                            </li>
                            <li>
                                <a href="#toptweets">Tweets</a>
                            </li>
                            <li>
                                <a href="#followers">Followers</a>
                            </li>
                        </ul>
                    </nav>
                    <!--End of Main Navigation-->

                    <div id="nav-trigger"><span></span></div>
                    <nav id="nav-mobile"></nav>

                </div>
            </div>
        </div><!--End of Header-->

        <!--Banner Content-->
        <div id="banner-content" class="row clearfix">

            <div class="col-38">

                <div class="section-heading">
                    <h1>GO-JEK</h1>
                    <h2>Social Media Emotion - Dashboard</h2>
                </div>

            </div>

        </div><!--End of Row-->
    </header>

    <!--Main Content Area-->
    <main id="content">

        <!--Introduction-->
        <section id="emotion" class="introduction scrollto">

            <div class="row clearfix">

                <div class="col-3">
                    <div class="section-heading">
                        <h3>SUCCESS</h3>
                        <h2 class="section-title">Emotion</h2>
                        <p class="section-subtitle">Overall Emotion</p>
                    </div>

                </div>


            </div>


        </section>
        <!--End of Introduction-->
        
        <!--Introduction-->
        <section id="featurebasedemotion" class="introduction scrollto">

            <div class="row clearfix">

                <div class="col-3">
                    <div class="section-heading">
                        <h3>SUCCESS</h3>
                        <h2 class="section-title">Emotion</h2>
                        <p class="section-subtitle">Feature-based Emotion</p>
                    </div>

                </div>

                <div class="col-2-3">

                </div>

            </div>


        </section>
        <!--End of Introduction-->
        
        <!--Introduction-->
        <section id="toptweets" class="introduction scrollto">

            <div class="row clearfix">

                <div class="col-3">
                    <div class="section-heading">
                        <h2 class="section-title">Most Influencial Tweets</h2>
                        <p class="section-subtitle">Period: </p>
                    </div>

                </div>

                <div class="col-2-3">

                </div>

            </div>


        </section>
        <!--End of Introduction-->
        
        <!--Introduction-->
        <section id="followers" class="introduction scrollto">

            <div class="row clearfix">

                <div class="col-3">
                    <div class="section-heading">
                        <h3>SUCCESS</h3>
                        <h2 class="section-title">Followers</h2>
                        <p class="section-subtitle">Top Followers of the week</p>
                    </div>

                </div>

                <div class="col-2-3">
                      <div class="panel-group">
                        <div class="panel panel-primary">
                          <div class="panel-heading">New / Churn followers</div>
                          <div class="panel-body">
                              <div class="table-responsive">
                                  <ul class="nav nav-tabs">
                                    <li class="active"><a data-toggle="tab" href="#new">New followers</a></li>
                                    <li><a data-toggle="tab" href="#churn">Churn followers</a></li>
                                  </ul>

                                  <div class="tab-content">
                                    <div id="new" class="tab-pane fade in active">
                                      <table class="table">
                                          <thead>
                                            <tr>
                                              <th>#</th>
                                              <th>Name</th>
                                              <th>Total Mention</th>
                                            </tr>
                                          </thead>
                                          <tbody>
                                            <tr>
                                              <td>1</td>
                                              <td>New York</td>
                                              <td>USA</td>
                                          </tr>
                                        </tbody>
                                      </table>
                                    </div>
                                    <div id="churn" class="tab-pane fade">
                                      content
                                    </div>
                                  </div>  
                              </div>
                          </div>
                        </div>
                      </div>
                </div>

            </div>


        </section>
        <!--End of Introduction-->
        <section id="x" class="introduction scrollto">
        <div  class="row clearfix">
             <div class="col-3">
                    <div class="section-heading">
                        <h3> </h3>
                    </div>
                </div>
        </div>
        </section>

    </main>
    <!--End Main Content Area-->


    <!--Footer-->
    <footer id="landing-footer" class="clearfix">
        <div class="row clearfix">

            <p id="copyright" class="col-2">NLP Team - GO-SQUADS TECH </p>

            <!--End of Social Icons in Footer-->
        </div>
    </footer>
    <!--End of Footer-->

</div>

<!-- Include JavaScript resources -->
<script src="js/jquery.1.8.3.min.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/featherlight.min.js"></script>
<script src="js/featherlight.gallery.min.js"></script>
<script src="js/jquery.enllax.min.js"></script>
<script src="js/jquery.scrollUp.min.js"></script>
<script src="js/jquery.easing.min.js"></script>
<script src="js/jquery.stickyNavbar.min.js"></script>
<script src="js/jquery.waypoints.min.js"></script>
<script src="js/images-loaded.min.js"></script>
<script src="js/lightbox.min.js"></script>
<script src="js/site.js"></script>


</body>
</html>